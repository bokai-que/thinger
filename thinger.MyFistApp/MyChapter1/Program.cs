﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyChapter01
{ 
    internal class Program
    {
        static void Main(string[] args)
        {
            // 实现两个整数相加
            Console.WriteLine("正在测试C#调用VB.NET类库！");
            MyChapter01.MyCalculator myCalculator = new MyChapter01.MyCalculator();
            int result = myCalculator.Add(10, 20);
            Console.WriteLine("纯C#程序计算  10+20=" + result);

            //调用VB.NET写的程序 ( 完全限定名)
            VBLibDemo.MyCalculator vbcal = new VBLibDemo.MyCalculator();
            int resultVB =vbcal.Add(50, 60);
            Console.WriteLine("调用VB.NET程序  50+60=" + resultVB);
            Console.ReadLine();
        }
    }
}
